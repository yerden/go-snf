module github.com/yerden/go-snf

go 1.12

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/google/gopacket v1.1.16
	golang.org/x/net v0.0.0-20190322120337-addf6b3196f6
)
